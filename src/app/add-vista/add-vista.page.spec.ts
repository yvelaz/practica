import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVistaPage } from './add-vista.page';

describe('AddVistaPage', () => {
  let component: AddVistaPage;
  let fixture: ComponentFixture<AddVistaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVistaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVistaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
