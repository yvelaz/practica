import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {AuthenticationService} from '../authentication.service';
import { Respuesta1 } from '../respuesta';
import { Avistamiento } from '../usuario';
import { ActivatedRoute, Router} from '@angular/router';
import {LoadingControllerService} from '../loading-controller.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-add-vista',
  templateUrl: './add-vista.page.html',
  styleUrls: ['./add-vista.page.scss'],
})
export class AddVistaPage implements OnInit {

  private id: string;
  private avist: Avistamiento = { place: '', long: "0.0", lat: "0.0" };
  private answer: Respuesta1;

  constructor(private routerActivate: ActivatedRoute,
              private loadingCtrl: LoadingController, 
              private restService:AuthenticationService,
              private router: Router,
              private loading: LoadingControllerService,
              private location: Location) { }

  ngOnInit() {
    this.getData();
  }

  getData(){
    //obtener el id del ave. 
    //Necesario inyectar ActivatedRoute
    //Necesario especificar parámetro :id en app-routing.module
    this.routerActivate.params.subscribe(params => {
      this.id = params['id']; 
      console.log(this.id );
    });
    
  } //getData

  goBack(){
    this.loading.loaderToShow = this.loadingCtrl.create({
      message: 'Obteniendo Datos del ave' 
      }).then((res) => {
        res.present().then(() => {
          //Volver a la página anterior
          this.goBack2();
        });
      });
 }

 goBack2(){
  
    //Volver a la página anterior
    this.location.back();

}

postAvist(){

  // onError Callback receives a PositionError object
  //
  function onError(error) {
      alert('code: '    + error.code    + '\n' +
            'message: ' + error.message + '\n');
  }

  navigator.geolocation.getCurrentPosition(
    (position) => 
    {
      this.avist.long = String(position.coords.longitude);
      this.avist.lat = String(position.coords.latitude);

      console.log("latitud: " + this.avist.lat  + ", longitud: " +  this.avist.long  );

      this.postAvist2();
    }
    
    , onError);
}

 postAvist2(){
  //Mostrar preloader
  this.loading.loaderToShow = this.loadingCtrl.create({
    message: 'Guardando avistamiento'
    }).then((res) => {
      res.present();
    });

   console.log("addAvistamiento " + this.id + " " + this.avist.place);
  this.restService.addAvistamiento(this.id, this.avist.place, this.avist.long, this.avist.lat)
  .subscribe( res => {
   // console.log(res);
    //(data: Respuesta) => {
      this.answer = {
        status: res['status'],
      }
      console.log(res);


      if (this.answer.status == "OK") {
        //mostrar un mensaje toast
        this.loading.presentToast('¡Avistamiento guardado correctamente!', 2000);

        //redirigir a la listas de aves
        //Dejar un segundo para mostrar los mensajes
        setTimeout(() => {
          console.log(this.id);
          this.goBack2();
        }, 1000)

      }
      else  //mostrar mensaje de error
      {
        //ocultar preloader
        this.loading.hideLoader();
        this.loading.presentFailedAlert('Error en el servidor', 'No se ha podido guardar el avistamiento. Pruebe más tarde.');
      }
    } //(res)
      ,
    (error) =>{
      console.error(error);
      //ocultar preloader
      this.loading.hideLoader();

      //mostrar mensaje de error
      this.loading.presentFailedAlert('Error en el servidor', 'No se ha podido guardar el avistamiento. Pruebe más tarde.');
      
    } // (error) 
    ); //addBird.subscribe
 }

 async ionViewDidEnter(){
  console.log("ionViewDidEnter");

  //ocultar preloader
 this.loading.hideLoader();

}

}
