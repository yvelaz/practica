import { Component, OnInit, Input } from '@angular/core';
import {avistList} from '../respuesta';

@Component({
  selector: 'app-item-avistar',
  templateUrl: './item-avistar.component.html',
  styleUrls: ['./item-avistar.component.scss'],
})
export class ItemAvistarComponent implements OnInit {
  @Input() selectedAvistar: avistList;

  constructor() { }

  ngOnInit() {}

}
