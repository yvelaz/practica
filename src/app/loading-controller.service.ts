import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { AlertController, ToastController  } from '@ionic/angular';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoadingControllerService {

  loaderToShow: any;
  constructor(public loadingController: LoadingController,
              private alertCtrl: AlertController,
              private toast:ToastController) { }


/*
  showLoader(mensaje) {
    console.log('showing preloader');
    this.loaderToShow = this.loadingController.create({
      message: mensaje 
    }).then((res) => {
      res.present();
 
      res.onDidDismiss().then((dis) => {
        console.log('Loading dismissed!');
      });
    });
    
    //this.hideLoader();
  }
 */

  hideLoader() {
    setTimeout(() => {
      this.loadingController.dismiss().catch(() => {});
    }, 500);
    console.log('hiding preloader');
  }

  async presentFailedAlert(titulo, mensaje) {
    let alert = await this.alertCtrl.create({
      header: titulo,
      subHeader: mensaje,
      buttons: ['OK']
    });
    await alert.present();
  }

  /*
  async presentFailedAlert() {
    let alert = await this.alertCtrl.create({
      header: 'Login Unsuccesful',
      subHeader: 'The login has failed. Please try to login again.',
      buttons: ['OK']
    });
    await alert.present();
  }
*/

async presentToast(mensaje, time) {
  const toast = await this.toast.create({
    message: mensaje,
    duration: time
  });
  toast.present();
}


}
