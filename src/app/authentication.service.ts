import { Injectable } from '@angular/core';
// importación imprescindible del módulo httpclient
// importación de utilidades necesarias para tratar los datos
//import {Observable} from 'rxjs/Observable';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse, HttpParams } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Respuesta } from './respuesta';
import {Usuario, Bird, Avistamiento} from './usuario';



const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'})
};
const apiUrl = "http://dev.contanimacion.com/birds/public/login/?";
const addBirdUrl = "http://dev.contanimacion.com/birds/public/addBird/?";
const addAvistURL = "http://dev.contanimacion.com/birds/public/addSighting/?";
const BirdListUrl = "http://dev.contanimacion.com/birds/public/getBirds/";
const BirdDetail = "http://dev.contanimacion.com/birds/public/getBirdDetails/";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private usuario :Usuario;
  public userEmail: string;

  constructor(private http: HttpClient) { }
  
    //Envía un nombre y contraseña al servicio web simulado
    //Devuelve la respuesta
    getLoginResponse(name, passw){

      this.userEmail = name;
      
      let httpBody = new HttpParams()
      .set('user', name)
      .set('password', passw);

      return this.http.post<Usuario>(apiUrl, httpBody, httpOptions);
  }

  //Envía un ave nueva
  addBird(id, name, description, location, long, lat){
    let httpBody;
    if (location == "") {
      console.log ("inside setAddbird");
        httpBody = new HttpParams()
        .set('idUser', id)
        .set('bird_name', name)
        .set('bird_description', description);
      }
      else {
        console.log ("inside setAddbird");
        httpBody = new HttpParams()
        .set('idUser', id)
        .set('bird_name', name)
        .set('bird_description', description)
        .set('place', location)
        .set('long', long)
        .set('lat', lat);
      }

      return this.http.post<Bird>(addBirdUrl, httpBody, httpOptions)
  }

  //Guarda un avistamiento
  addAvistamiento(id, location, long, lat){

    let httpBody;

    httpBody = new HttpParams()
    .set('idAve', id)
    .set('place', location)
    .set('long', long)
    .set('lat', lat);


      return this.http.post<Avistamiento>(addAvistURL, httpBody, httpOptions);
  }


  private extractData(res: Response) {
    console.log("extractData...");
    console.log (res[0]);
    let body = res;
    return body || {};
  }

  //Obtener lista de pájaros
  public getListOfBirds(id): Observable<any> {

    let url = BirdListUrl + id;
    // Call the http GET
    return this.http.get(url, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  //Obtener detalle de pájaro
  public getBirdDetail (id): Observable<any> {

    let url = BirdDetail + id;

    // Call the http GET
    return this.http.get(url, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );

  }



  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

}

