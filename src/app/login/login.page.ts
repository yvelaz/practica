import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../authentication.service';
import { Respuesta } from '../respuesta';
import {LoadingControllerService} from '../loading-controller.service';
import {Router} from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  registerCredentials = { name: '', password: '' };
  //users: any[] = [];
  answer: Respuesta;

  constructor(private loading: LoadingControllerService,
              private loadingCtrl:LoadingController,
              private restService:AuthenticationService,
              public router: Router) { }

  ngOnInit() {
  }



  public login() {

    //Mostrar preloader
    this.loading.loaderToShow = this.loadingCtrl.create({
      message: 'Comprobando usuario y password'
      }).then((res) => {
        res.present();
      });

    //Obtener id de usuario con el nombre y password informados
    this.restService.getLoginResponse(this.registerCredentials.name,this.registerCredentials.password)
    .subscribe( res => {
        this.answer = {
          status: res['status'],
          id:  res['id']
        }
        console.log(res);

        
        //Comprobar si el login ha ido bien. El id debe ser distinto de blancos
        //he añadido setTimeout para que se pueda ver el preloader y no se muestre la alerta y el preloader a la vez
        if (this.answer.id == "") {
          //ocultar preloader
          this.loading.hideLoader();
          setTimeout(() => {
            this.loading.presentFailedAlert('Error de acceso', 'El acceso ha fallado. Pruebe otra vez.');
          }, 500);
        }
        else {
          //mostrar un mensaje toast
          this.loading.presentToast('¡Datos de acceso correctos!', 2000);

          //redirigir a la pantalla principal
          //Dejar un segundo para mostrar los mensajes
          setTimeout(() => {
            console.log(this.answer.id);
            this.router.navigate(['/home', this.answer.id]);
          }, 1000)

        }
        
      } // (res =>)
      ,
      (error) =>{
        console.error(error);
        this.loading.hideLoader();
        //mostrar mensaje de error
        this.loading.presentFailedAlert('Error de acceso', 'El acceso ha fallado. Pruebe otra vez.');
      } //(error)

    ); //getLoginResponse.subscribe
  } //login()


}
