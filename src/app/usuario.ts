export class Usuario {
    user: string;
    password: string;
}

export class Bird {
    idUser: string;
    bird_name: string;
    bird_description: string;
}

export class Avistamiento {
    place: string;
    long: string;
    lat: string;
}

