import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'home/:id', loadChildren: './home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'add-ave/:id', loadChildren: './add-ave/add-ave.module#AddAvePageModule' },
  { path: 'list-ave/:id', loadChildren: './list-ave/list-ave.module#ListAvePageModule' },
  { path: 'detail-ave/:id', loadChildren: './detail-ave/detail-ave.module#DetailAvePageModule' },
  { path: 'add-vista/:id', loadChildren: './add-vista/add-vista.module#AddVistaPageModule' },
  // esta última sirve para ofrecer una ruta que se abra en el raiz de la app
  { path: '', redirectTo: '/login', pathMatch: 'full' }
  //{ path: 'list-item', loadChildren: './list-item/list-item.module#ListItemPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
