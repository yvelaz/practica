import { Component } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
//import { Location } from '@angular/common';
import {LoadingControllerService} from '../loading-controller.service';
import { LoadingController } from '@ionic/angular';
import {AuthenticationService} from '../authentication.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  private id: string;
  private userEmail: string;

  constructor(private loadingCtrl: LoadingController, 
              private loading: LoadingControllerService,
              private routerActivate: ActivatedRoute,
              private restService:AuthenticationService,
              private router: Router) { }

  ngOnInit() {
    
    //necesario llamar a otra función
    this.getData();
  }


  getData(){
    //obtener el id de usuario. Este id se pasa desde login.page.ts
    //Necesario inyectar ActivatedRoute
    //Necesario especificar parámetro :id en app-routing.module
    this.routerActivate.params.subscribe(params => {
      this.id = params['id']; 
      this.userEmail = this.restService.userEmail;
    });
    
  } //getData


  listadoClick() {
    console.log("this is " + this.id);

    this.loading.loaderToShow = this.loadingCtrl.create({
      message: 'Obteniendo lista de aves' 
      }).then((res) => {
        res.present().then(() => {
          this.router.navigate(['/list-ave', this.id]);
        });
      });
    
  }

  addClick(){
    console.log("this is " + this.id);
    this.router.navigate(['/add-ave', this.id]);

  }

  desconectarClick()
  {
    this.router.navigate(['/login']);
  }

  async ionViewDidEnter(){
    console.log("ionViewDidEnter");

    //ocultar preloader
   this.loading.hideLoader();

}
}
