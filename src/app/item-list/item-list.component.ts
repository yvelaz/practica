import { Component, OnInit, Input } from '@angular/core';
import {birdsjson} from '../respuesta';
import {Router} from '@angular/router';
import {LoadingControllerService} from '../loading-controller.service';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss'],
})
export class ItemListComponent implements OnInit {
  @Input() selectedBird: birdsjson;
  
  constructor(private loadingCtrl: LoadingController, 
              private loading: LoadingControllerService,
              public router: Router) { }

  ngOnInit() {}


  goDetail(): void {
    console.log("goDetail " + this.selectedBird.id);
   // this.loading.showLoader('Obteniendo lista de aves');

    this.loading.loaderToShow = this.loadingCtrl.create({
      message: 'Obteniendo Datos del ave' 
      }).then((res) => {
        res.present().then(() => {
  
          this.router.navigate(['/detail-ave', this.selectedBird.id]);
        });
      });

    //Navegar a la página detail
    //this.router.navigate(['/detail-ave', this.selectedBird.id]);
    
  }

  getImage() {
    return this.selectedBird.bird_image;
  }
  
}
