import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../authentication.service';
import {birdsjson} from '../respuesta';
import {LoadingControllerService} from '../loading-controller.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-list-ave',
  templateUrl: './list-ave.page.html',
  styleUrls: ['./list-ave.page.scss'],
})
export class ListAvePage implements OnInit {
  loaderToShow: any;
  private id;
  birds: birdsjson[] = [];

  
  constructor(private loading: LoadingControllerService,
              public loadingController: LoadingController,
              private routerActivate: ActivatedRoute,
              private restService:AuthenticationService,
              private router: Router) { }

  ngOnInit() {
    //necesario llamar a otra función
    this.getData();
  }

  getData(){

    //obtener el id de usuario. Este id se pasa desde login.page.ts
    //Necesario inyectar ActivatedRoute
    //Necesario especificar parámetro :id en app-routing.module
    this.routerActivate.params.subscribe(params => {
     
      this.id = params['id']; 
      console.log ("show Preloader");
      
      this.restService.getListOfBirds(this.id)
      .subscribe( res => { 
        console.log(res);
        this.birds = res;
        //ocultar preloader
         this.loading.hideLoader();

      });

    }
    ,
    (error) =>{
      console.error(error);
      //mostrar mensaje de error
      this.loading.presentFailedAlert('Error de conexión', 'Ha fallado la conexión al servidor. Pruebe más tarde.');

      //ocultar preloader
      this.loading.hideLoader();

    } //(error)

    );
    
  } //getData

  homeClick(){
    this.router.navigate(['/home', this.id]);
  }

  async ionViewDidEnter(){

    //ocultar preloader
   this.loading.hideLoader();

}

}
