import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListAvePage } from './list-ave.page';

//IMPORTANTE: importar el componente que se vaya a usar en el html 
import {ItemListComponent} from '../item-list/item-list.component';

const routes: Routes = [
  {
    path: '',
    component: ListAvePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  //IMPORTANTE: declarar el componente que se vaya a usar en el html 
  declarations: [ListAvePage, ItemListComponent]
})
export class ListAvePageModule {}
