import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAvePage } from './list-ave.page';

describe('ListAvePage', () => {
  let component: ListAvePage;
  let fixture: ComponentFixture<ListAvePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAvePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAvePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
