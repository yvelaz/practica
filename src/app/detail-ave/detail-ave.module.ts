import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetailAvePage } from './detail-ave.page';

//IMPORTANTE: importar el componente que se vaya a usar en el html 
import {ItemAvistarComponent} from '../item-avistar/item-avistar.component';

const routes: Routes = [
  {
    path: '',
    component: DetailAvePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetailAvePage, ItemAvistarComponent]
})
export class DetailAvePageModule {}
