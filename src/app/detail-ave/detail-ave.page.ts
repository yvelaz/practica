import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { Location } from '@angular/common';
import {AuthenticationService} from '../authentication.service';
import { birdDetail } from '../respuesta';
import {LoadingControllerService} from '../loading-controller.service';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-detail-ave',
  templateUrl: './detail-ave.page.html',
  styleUrls: ['./detail-ave.page.scss'],
})
export class DetailAvePage implements OnInit {

  private id;
  answer : birdDetail;
  private cargando;


  constructor(private loadingCtrl: LoadingController,
              private loading: LoadingControllerService,
              private routerActivate: ActivatedRoute,
              private router: Router,
              private restService:AuthenticationService,
              private location: Location) { }

  ngOnInit() {

    this.getData();
  }

  getData(){

    //obtener el id de usuario. Este id se pasa desde login.page.ts
    //Necesario inyectar ActivatedRoute
    //Necesario especificar parámetro :id en app-routing.module
    this.routerActivate.params.subscribe(params => {
      this.id = params['id']; 
      console.log("Lista de pájaros. ID:" + this.id);
      
      this.restService.getBirdDetail(this.id)
      .subscribe( res => { 
        console.log(res[0]);
        this.answer = res[0];

      });

    });
    
  } //getData


  getImage(){

    //if ((this.answer))
    if (this.answer) {
      return this.answer.bird_image;
    }
    return "";
  }  

  
  goBack(){

    this.loading.loaderToShow = this.loadingCtrl.create({
      message: 'Obteniendo Datos del ave' 
      }).then((res) => {
        res.present().then(() => {
  
          //Volver a la página anterior
          this.location.back();
        });
      });


  }

  addClick(){
    this.router.navigate(['/add-vista', this.answer.id]);
  }


  async ionViewWillEnter(){
    console.log("ionViewWillEnter")
    
  }

 async ionViewDidEnter(){
      console.log("ionViewDidEnter");

      //ocultar preloader
     this.loading.hideLoader();

  }

}
