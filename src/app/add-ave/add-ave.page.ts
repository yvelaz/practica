import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { Location } from '@angular/common';
import {AuthenticationService} from '../authentication.service';
import { Respuesta1 } from '../respuesta';
import {LoadingControllerService} from '../loading-controller.service';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-add-ave',
  templateUrl: './add-ave.page.html',
  styleUrls: ['./add-ave.page.scss'],
})
export class AddAvePage implements OnInit {

  private id;
  answer: Respuesta1;
  ave = { nombre: '', descripcion: '', esAvistado:false, lugar:'', longitud:0.0, altitud:0.0 };

  constructor(private loading: LoadingControllerService,
              private loadingCtrl: LoadingController, 
              private routerActivate: ActivatedRoute,
              private router: Router,
              private restService:AuthenticationService,
              private location: Location) { }

  ngOnInit() {
    //necesario llamar a otra función
    this.getData();
  }

  getData(){
    //obtener el id de usuario. Este id se pasa desde login.page.ts
    //Necesario inyectar ActivatedRoute
    //Necesario especificar parámetro :id en app-routing.module
    this.routerActivate.params.subscribe(params => {
      this.id = params['id']; 

    });
    
  } //getData

  goBack(){

    this.loading.loaderToShow = this.loadingCtrl.create({
      message: 'Obteniendo Datos del ave' 
      }).then((res) => {
        res.present().then(() => {
          //Volver a la página anterior
          this.location.back();
        });
      });
        
  }

  añadirAve(){
    console.log("NO es avistado");

    //Mostrar preloader
    this.loading.loaderToShow = this.loadingCtrl.create({
      message: 'Guardando ave en el servidor'
      }).then((res) => {
        res.present();
      });

    this.restService.addBird(this.id, this.ave.nombre,this.ave.descripcion, this.ave.lugar, this.ave.longitud, this.ave.altitud)
    .subscribe( res => {
        this.answer = {
          status: res['status'],
        }
        console.log(res);


        if (this.answer.status == "OK") {
          //mostrar un mensaje toast
          this.loading.presentToast('¡Se ha guardado correctamente!', 2000);


          //redirigir a la listas de aves
          //Dejar un segundo para mostrar los mensajes
          setTimeout(() => {
            console.log(this.id);
            this.router.navigate(['/list-ave', this.id]);
          }, 1000)

        }
        else {
          //ocultar preloader
          this.loading.hideLoader();
          this.loading.presentFailedAlert('Error de servidor', 'Ha habido un error al guardar. Pruebe otra vez después de unos minutos.');
        }
        } //(res)
        ,
      (error) =>{
        //ocultar preloader
        this.loading.hideLoader();
        //mostrar mensaje de error
        this.loading.presentFailedAlert('Error de servidor', 'Ha habido un error al guardar. Pruebe otra vez después de unos minutos.');
        console.error(error);
        
      } // (error) 
      ); //addBird.subscribe

  }



  postAve(){
    if (this.ave.esAvistado){
      console.log("es avistado");
      navigator.geolocation.getCurrentPosition(
        (position) => 
        {
          this.ave.longitud = position.coords.longitude;
          this.ave.altitud = position.coords.latitude;
    
          console.log("latitud: " + this.ave.longitud  + ", longitud: " +  this.ave.altitud );
    
          this.añadirAve();
        }
        
        ,  (error) =>{
          console.error(error);});
    } 
    else {
      //Borrar datos anteriores de avistamiento
      this.ave.lugar = "";
      this.ave.longitud = 0;
      this.ave.altitud = 0;

      console.log("NOOO es avistado");
      this.añadirAve();
    }

  } //postAve

  async ionViewDidEnter(){
    console.log("ionViewDidEnter");

    //ocultar preloader
   this.loading.hideLoader();

  }

}
