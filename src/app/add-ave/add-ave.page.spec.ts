import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAvePage } from './add-ave.page';

describe('AddAvePage', () => {
  let component: AddAvePage;
  let fixture: ComponentFixture<AddAvePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAvePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAvePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
