//import { ExecFileOptionsWithStringEncoding } from 'child_process';

export class Respuesta {
    status: string;
    id: string;
}


export class Respuesta1 {
    status: string;
}

export class birdsjson {
    id: string;
    bird_name: string;
    bird_image: string;
    bird_sightings: string;
    mine: number;
}

export class avistList {
    id: string;
    idAve: string;
    place: string;
    long: string;
    lat: string;
}

export class birdDetail {
    id: string;
    idUser: string;
    bird_image: string;
    bird_name: string;
    bird_description: string;
    bird_sightings: number;
    sightings_list: avistList;
}